import Classification from '../Classification/Classification';
import style from './style.module.scss';

const ACB = ({ risk = 'notice', containment = 'safe', disruption = 'dark', secondary }) => {
    return <svg
        width="100%"
        viewBox="0 0 100 100"
        fill="none"
        xmlns="http://www.w3.org/2000/svg">
            <defs>
            <style type="text/css">
                {"@font-face {font-family: Inter;src: url('https://cdn.jsdelivr.net/gh/Nu-SCPTheme/Black-Highlighter@gh-pages/fonts/Inter.var.woff2?v=3.19')}"}
            </style>
            </defs>
        <Classification risk={risk} containment={containment} disruption={disruption} secondary={secondary}  />
    </svg>
}

export default ACB;