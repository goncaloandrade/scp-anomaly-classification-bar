import { color, num } from "../../styles/vars";
import IconCircle from "../IconCircle/IconCircle";

const IconNumber = ({ x = 0, y = 0, element}) => {
    return <svg
        x={x}
        y={y}
        overflow={'hidden'}
        viewBox={`0 0 100 30`}>
        <rect
            x={15}
            width={20}
            height={30}
            fill={color.neutral}
         />
        <circle
            cx={15}
            r={15}
            cy={15}
            fill={color.neutral}
        />
        <circle
            cx={35}
            r={15}
            cy={15}
            fill={color.neutral}
        />
        <text
            x={10}
            y={20}
            style={{
                fontSize: 14,
                fontFamily: 'Inter',
                textTransform: 'uppercase'
            }}
            fill='white'>{num[element]}</text>
        <IconCircle
            x={20}
            y={0}
            width={30}
            element={element} />
    </svg>
}

export default IconNumber;