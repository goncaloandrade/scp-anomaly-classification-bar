import { color, map } from "../../styles/vars";
import IconCircle from "../IconCircle/IconCircle";
import IconNumber from "../IconNumber/IconNumber";

const Label = ({ x = 0, y = 0, width = 30, label = 'Containment Class:', value = 'safe', mode = 'primary'}) => {
    const fontSize = 4,
        height = mode === 'primary' ? 20 : 10,
        secondaryFontSize = fontSize * (mode === 'primary' ? 2.5 : 1.25);
    return <svg
        x={x}
        y={y}
        width={width}
        overflow={'hidden'}
        viewBox={`0 0 100 ${height}`}>
        <rect
            width="100%"
            height="100%"
            fill={color[`${map[value] || 'neutral'}Light`]}
        />
        <rect
            x="0"
            y="0"
            width={mode === 'primary' ? 5 : 3}
            height={height}
            fill={color[map[value] || 'neutral']}
        />
        <text
            x={6.5}
            y={6.5}
            style={{
                fontSize,
                fontFamily: 'Inter',
                textTransform: 'uppercase'
            }}
            fill={color.neutral}>
            <tspan>{label}</tspan>
            <tspan
                x={mode === 'primary' ? 6 : undefined}
                dx={mode === 'primary' ? undefined : 1}
                dy={mode === 'primary' ? fontSize * 2.2 : fontSize * .05}
                style={{
                    fontSize: secondaryFontSize,
                    fontWeight: 600,
                }}>{value}</tspan>
        </text>
        {mode === 'primary' ?
            <IconCircle
                x={82}
                y={3}
                width={15}
                element={value} /> :
            <IconNumber
                x={48}
                y={0}
                width={35}
                element={value}
        />}
    </svg>
}

export default Label;