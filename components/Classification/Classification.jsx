import { color, map } from "../../styles/vars";
import Label from "../Label/Label";
import Rose from "../Rose/Rose";

const Classification = ({
    risk = 'notice',
    containment = 'safe',
    disruption = 'dark',
    secondary 
    
}) => {
    return <svg
        viewBox="0 0 100 30"
        fill="none"
        xmlns="http://www.w3.org/2000/svg">
        <Label label="containment class:" y={-8.5} value={containment} width={66} mode={'primary'} />
        <Label label="disruption class:" y={3.25} value={disruption} width={66} mode={'secondary'} />
        <Label label="risk class:" y={11.75} value={risk} width={66} mode={'secondary'} />
        <rect x={66} width={2} height="100%" fill={color.neutral} />
        <Rose x={70} risk={risk} containment={containment} disruption={disruption} secondary={secondary} />
    </svg>
}

export default Classification;