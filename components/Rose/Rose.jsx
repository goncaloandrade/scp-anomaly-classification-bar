import { color, map } from '../../styles/vars';
import IconCircle from '../IconCircle/IconCircle';

const Rose = ({ x = 0, y = 0, width = 30, height = 30, risk = 'notice', containment = 'safe', disruption = 'dark', secondary }) => {
    return <svg
            x={x}
            y={y}
            width={width}
            viewBox='0 0 30 30'>
        <g id="risk">
            <path id="risk-background" fill={color[`${map[risk]}Light`]} d="M29.462 20.3923V9.65373L24.8774 5.13037L15 15.0078L24.8927 24.9005L29.462 20.3923Z" />
            <IconCircle element={risk} x={19} y={10.25} width={9.5} />
        </g>
        <g id="containment">
            <path id="containment-background" fill={color[`${map[containment]}Light`]} d="M20.3537 0.530334H9.61514L5.0918 5.11496L14.9691 14.9923L24.8618 5.09959L20.3537 0.530334Z" />
            <IconCircle element={containment} x={10.25} y={1.5} width={9.5} />
        </g>
        <g id="disruption">
            <path id="disruption-background" fill={color[`${map[disruption]}Light`]} d="M0.491699 20.3923V9.65373L5.07633 5.13037L14.9537 15.0078L5.06096 24.9005L0.491699 20.3923Z" />
            <IconCircle element={disruption} x={2} y={10.25} width={9.5} />
        </g>
        {secondary && <g id="secondary">
            <IconCircle element={secondary} x={10.25} y={19} width={9.5} />
        </g>}
        
        <path id="outline" stroke={color.neutral} strokeWidth={1} d="M5.10732 5.11496L0.568604 9.65368V20.3922L5.09207 24.9157M5.10732 5.11496L9.64606 0.576234L20.2616 0.576233L24.8774 5.13032M5.10732 5.11496L0.568604 0.576233M5.10732 5.11496L15 15.0077M5.09207 24.9157L9.64606 29.4697H20.2616L24.8928 24.9005M5.09207 24.9157L15 15.0077M5.09207 24.9157L0.568604 29.4392M24.8774 5.13032L29.462 9.65368V20.3922L24.8928 24.9005M24.8774 5.13032L15 15.0077M24.8774 5.13032L29.37 0.637708M0.568604 0.576233H1.55221L0.568604 1.55984V0.576233ZM15 15.0077L24.8928 24.9005M24.8928 24.9005L29.462 29.4697M29.462 29.4697V28.4861L28.4784 29.4697H29.462ZM29.37 0.637708V1.43689L28.5709 0.637708H29.37ZM0.568604 29.4392H1.49074L0.568604 28.5171V29.4392Z" />
    </svg>


}

export default Rose;