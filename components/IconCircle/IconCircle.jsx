import { color, map } from '../../styles/vars';

const IconCircle = ({ element, x = 0, y = 0, width = 100, height = width }) => <svg
    width={width}
    height={height}
    x={x}
    y={y}
    viewBox="0 0 100 100"
    fill="none"
    xmlns="http://www.w3.org/2000/svg">
        <circle id="circle" fill={color[map[element]]} stroke={color.neutral} strokeWidth={10} cx={50} cy={50} r={44} />
        <image href={`/icon/${element}.svg`} x={6} y={6} height={88} width={88} />
    </svg>;

export default IconCircle;